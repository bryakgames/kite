using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Rigidbody2D))]
public class KiteHead : MonoBehaviour
{

    [SerializeField]
    public Vector3 center;
    private GameObject centerObject;

    private WindAffected windAffected;
    private ScoreCollector scoreCollector;
    private CanBeDamaged canBeDamaged;

    public void Initialize(ScoreHolder scoreHolder, HealthHolder healthHolder, WindController controller) {
        this.windAffected = this.gameObject.AddComponent<WindAffected>();
        this.windAffected.controller = controller;
        this.scoreCollector = this.gameObject.AddComponent<ScoreCollector>();
        this.scoreCollector.holder = scoreHolder;
        this.canBeDamaged = this.gameObject.AddComponent<CanBeDamaged>();
        this.canBeDamaged.healthHolder = healthHolder;
        this.GetRigidbody2D().centerOfMass = center;
    }

    public Rigidbody2D GetRigidbody2D() {
        return this.GetComponent<Rigidbody2D>();
    }
}
