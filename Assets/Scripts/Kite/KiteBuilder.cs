using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class KiteBuilder : MonoBehaviour
{
    [SerializeField] public ScoreHolder scoreHolder;
    [SerializeField] public HealthHolder healthHolder;
    [SerializeField] public WindController wind;
    [SerializeField] public ThreadController thread;
    [SerializeField] public LineController line;
    /* Kite should be added to bonus spawners as a restriction */
    [SerializeField] public List<BonusSpawner> spawners;

    [Header("Kite Parts")]
    [SerializeField] public KiteHead head;
    [SerializeField] public KiteTail tail;
    [SerializeField] public KiteTail tailEnd;

    [Header("Kite Properties")]
    [SerializeField] public int tailLength;

    private KiteHead headInstance;
    private GameObject headCenter;
    private List<KiteTail> tailInstances;

    void Start() {
        this.Recreate();
    }

    public void Recreate() {
        this.CleanUp();
        this.CreateHead();
        this.CreateTail();
        this.ConnectThread();
    }

    public void CleanUp() {
        while (transform.childCount > 0) {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    private void CreateHead() {
        this.headInstance = Instantiate(this.head, this.transform.position, this.transform.rotation);
        this.headInstance.transform.parent = this.transform;
        this.headInstance.Initialize(this.scoreHolder, this.healthHolder, this.wind);
        this.headCenter = new GameObject("Center");
        this.headCenter.transform.parent = this.headInstance.transform;
        this.headCenter.transform.position = this.headInstance.center + this.headInstance.transform.position;
        if (this.spawners != null) {
            this.spawners.ForEach(_spawner => _spawner.restrictionZones = this.headCenter.transform);
        }
    }

    private void CreateTail() {
        Rigidbody2D prevSegment = this.headInstance.GetComponent<Rigidbody2D>();
        int partsToCreate = this.tailLength;
        while (partsToCreate > 0) {
            KiteTail tailPart = this.CreateKiteTailFragment(this.tail, prevSegment);
            prevSegment = tailPart.GetComponent<Rigidbody2D>();
            partsToCreate--;
        }
        this.CreateKiteTailFragment(this.tailEnd, prevSegment);
    }

    private KiteTail CreateKiteTailFragment(KiteTail tail, Rigidbody2D prevSegment) {
        SpriteRenderer tailRenderer = tail.GetComponent<SpriteRenderer>();
        SpriteRenderer prevSegmentRenderer = prevSegment.GetComponent<SpriteRenderer>();
        Vector3 bounds = tailRenderer.bounds.min - tailRenderer.transform.position;
        Vector3 position = new Vector3(this.transform.position.x, prevSegmentRenderer.bounds.min.y + bounds.y, 0);
        KiteTail tailPart = Instantiate(tail, position, this.headInstance.transform.rotation);
        // Initialize
        tailPart.transform.parent = this.headInstance.transform;
        tailPart.Initialize();
        // Add Joint
        HingeJoint2D joint = tailPart.gameObject.AddComponent<HingeJoint2D>();
        joint.connectedBody = prevSegment;
        joint.anchor = new Vector2(0, -bounds.y);
        //
        return tailPart;
    }

    private void ConnectThread() {
        if (this.thread.getJoint() != null) {
            this.thread.getJoint().connectedBody = this.headInstance.GetRigidbody2D();
            this.thread.getJoint().connectedAnchor = this.headInstance.center / 2;
        }
        this.line.SetUpLine(new Transform[]{this.headCenter.transform, this.thread.transform});
    }
}
