using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(LineRenderer))]
public class LineController : MonoBehaviour
{
    private LineRenderer lr;
    private Transform[] points;
    
    void Awake()
    {
        this.lr = this.GetComponent<LineRenderer>();
    }

    public void SetUpLine(Transform[] points) {
        lr.positionCount = points.Length;
        this.points = points;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < this.points.Length; i++) {
            var position = this.points[i] == null ? new Vector3(0, 0, 0) : this.points[i].position;
            this.lr.SetPosition(i, new Vector3(position.x, position.y, 0));
        }
    }
}
