using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpringJoint2D))]
public class ThreadController : MonoBehaviour
{

    [SerializeField] public float threadChangeSpeed = 1;
    [SerializeField] public float horizontalSpeed = 1;
    [SerializeField] public float minimalLength = 0.5f;
    [SerializeField] public float maximalLength = 7;
    [SerializeField] public float moveBorder = 0.5f;
    private float xMovement = 0;
    private float yMovement = 0;

    private Rigidbody2D body;
    private SpringJoint2D joint;
    private Animator anim;

    public SpringJoint2D getJoint() {
        return this.joint;
    }

    void Awake()
    {
        this.body = this.GetComponent<Rigidbody2D>();
        this.joint = this.GetComponent<SpringJoint2D>();
        this.anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // Horizontal movement
        this.xMovement += Input.GetAxis("Horizontal");
        if (Input.GetMouseButton(0)) {
            this.xMovement += Input.GetAxis("Mouse X");
        }
        // Thread change
        this.yMovement += Input.GetAxis("Vertical");
        this.yMovement += Input.GetAxis("Mouse ScrollWheel");
    }

    void FixedUpdate()
    {
        if (this.xMovement != 0) {
            float newPositionX = this.body.position.x + Mathf.Sign(this.xMovement) * this.horizontalSpeed;
            float screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x - this.moveBorder;
            if (newPositionX >= -screenWidth && newPositionX <= screenWidth)
            this.body.MovePosition(new Vector2(newPositionX, this.body.position.y));
            this.xMovement = 0;
        }
        if (this.yMovement != 0) {
            float newDistance = joint.distance + Mathf.Sign(this.yMovement) * this.threadChangeSpeed;
            if (newDistance >= this.minimalLength && newDistance <= maximalLength) {
                this.joint.distance = newDistance;
                this.anim.SetBool("rolling", true);
            }
            this.yMovement = 0;
        } else {
            this.anim.SetBool("rolling", false);
        }
    }
}
