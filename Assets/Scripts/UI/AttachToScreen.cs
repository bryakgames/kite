using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AttachToScreen : MonoBehaviour
{
    /// Value from 0..1, where 0 - left edge of screen, 1 - right
    public float screenX = 0f;
    /// Value from 0..1, where 0 - bottom edge of screen, 1 - top
    public float screenY = 0f;
    // Shift to compensate object width
    public float widthShift;
    // Shift to compensate object height
    public float heightShift;

    // Update is called once per frame
    void Update()
    {
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(new Vector3(screenX * Screen.width, screenY * Screen.height, 0));
        this.transform.position = new Vector3(worldPos.x - this.widthShift, worldPos.y - this.heightShift, 0);
    }
}
