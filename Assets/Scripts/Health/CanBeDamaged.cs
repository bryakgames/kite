using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CanBeDamaged : MonoBehaviour
{
    
    [SerializeField] public HealthHolder healthHolder;

    void OnCollisionEnter2D(Collision2D collisionInfo)
    {
        CanDamage domagir = collisionInfo.collider.GetComponent<CanDamage>();
        if (domagir != null && this.healthHolder.health > 0) {
            this.healthHolder.health -= domagir.damage;
        }
    }
}
