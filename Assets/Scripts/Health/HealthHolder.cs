using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthHolder : MonoBehaviour
{
    [SerializeField] public int maxHealth;
    [SerializeField] public int health;

    void Start()
    {
        this.health = this.maxHealth;
    }
}
