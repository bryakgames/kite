using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[ExecuteInEditMode]
public class HealthBar : MonoBehaviour
{
    [SerializeField] HealthHolder healthOwner;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        this.spriteRenderer = this.GetComponent<SpriteRenderer>();
        if (this.spriteRenderer.drawMode != SpriteDrawMode.Tiled) {
            throw new System.Exception("Linked SpriteRenderer should be in tile mode");
        }
    }

    private void Update() {
        if (this.spriteRenderer != null) {
            this.spriteRenderer.size = new Vector2(this.healthOwner.health, 1);
        }
    }

}
