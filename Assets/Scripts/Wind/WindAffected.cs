using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class WindAffected : MonoBehaviour
{

    public WindController controller;
    private Rigidbody2D rigBody;

    // Start is called before the first frame update
    void Start()
    {
        this.rigBody = this.GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        Vector2 force = new Vector2(Mathf.Cos(this.controller.windAngle * Mathf.PI / 180), Mathf.Sin(this.controller.windAngle * Mathf.PI / 180)) * this.controller.windPower;
        Debug.DrawRay(this.rigBody.position, force, Color.blue);
        this.rigBody.AddForce(force);
    }
}
