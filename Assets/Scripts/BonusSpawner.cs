using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpawner : MonoBehaviour
{

    [Header("Spawner Properties")]
    [SerializeField] GameObject objectExample;
    [SerializeField] int numberOfObjects;
    [SerializeField] int initialObjects;
    [SerializeField] float spawnerCooldownSeconds;
    private float spawnCooldownCounter;
    private List<GameObject> spawnedInstances;
    [Header("Spawner zone")]
    [SerializeField] float spawnRadius = 5;
    [SerializeField] float spawnXMultiplier = 1;
    [SerializeField] float spawnYMultiplier = 1;
    [Header("Restriction zone")]
    [SerializeField] public Transform restrictionZones;
    [SerializeField] float restrictionRadius;

    // Start is called before the first frame update
    void Start()
    {
        this.spawnedInstances = new List<GameObject>();
        this.spawnCooldownCounter = this.spawnerCooldownSeconds;
        while (this.spawnedInstances.Count < this.initialObjects) {
            this.spawnedInstances.Add(this.CreateObject());
        }
    }

    // Update is called once per frame
    void Update()
    {
        DrawSpawnZones();
        this.spawnedInstances.RemoveAll(cloud => cloud == null);
        if (this.spawnedInstances.Count < this.numberOfObjects) {
            if (this.spawnCooldownCounter > 0) {
                this.spawnCooldownCounter -= Time.deltaTime;
            }
            if (this.spawnCooldownCounter <=0) {
                this.spawnedInstances.Add(this.CreateObject());
                this.spawnCooldownCounter = this.spawnerCooldownSeconds;
            }
        }
    }

    private GameObject CreateObject() {
        var position = this.GenerateStartPosition();
        GameObject obj = Instantiate(this.objectExample, position, this.transform.rotation);
        return obj;
    }

    private Vector3 GenerateStartPosition() {
        Vector3 possiblePosition;
        do {
            possiblePosition = this.CalculatePosition(
                this.transform.position, 
                Random.Range(0, this.spawnRadius), Random.Range(0, 360),
                this.spawnXMultiplier, this.spawnYMultiplier
            );
        } while (Vector3.Distance(possiblePosition, this.restrictionZones.position) < this.restrictionRadius);
        
        return possiblePosition;
    }

    private void DrawSpawnZones() {
        this.DrawCircle(this.transform.position, Color.green, this.spawnRadius, this.spawnXMultiplier, this.spawnYMultiplier);
        if (this.restrictionZones != null) {
            this.DrawCircle(this.restrictionZones.transform.position, Color.red, this.restrictionRadius, 1, 1);
        }
    }

    private void DrawCircle(Vector3 pos, Color color, float radius, float xMultiplier, float yMultiplier) {
        int step = 15;
        Vector3 startSpawn;
        Vector3 endSpawn = this.CalculatePosition(pos, radius, 0, xMultiplier, yMultiplier);
        for (int i = step; i <= 360; i += step) {
            startSpawn = endSpawn;
            endSpawn = this.CalculatePosition(pos, radius, i, xMultiplier, yMultiplier);
            Debug.DrawLine(startSpawn, endSpawn, color);
        }

    }

    private Vector3 CalculatePosition(Vector3 startPosition, float radius, float angle, float xMultiplier, float yMultiplier) {
        return startPosition + new Vector3(
            radius * Mathf.Cos(angle * Mathf.PI / 180) * xMultiplier,
            radius * Mathf.Sin(angle * Mathf.PI / 180) * yMultiplier,
            0
        );
    }
}
