using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(KiteBuilder))]
public class KiteEditor : Editor {
    public override void OnInspectorGUI() {
        var builder = (KiteBuilder) target;

        EditorGUI.BeginChangeCheck();

        base.OnInspectorGUI();

        if (EditorGUI.EndChangeCheck()) {
            builder.Recreate();
        }
        
    }
}