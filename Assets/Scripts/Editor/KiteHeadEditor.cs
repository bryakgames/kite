using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(KiteHead))]
public class KiteHeadEditor : Editor {

    KiteHead head;

    private void OnSceneGUI() {
        this.Draw();
    }

    void Draw() {
        Handles.color = Color.green;
        Vector2 newPos = Handles.FreeMoveHandle(this.head.center + head.transform.position, Quaternion.identity, .1f, Vector2.zero, Handles.CylinderHandleCap);
        if (head.center.x != newPos.x || head.center.y != newPos.y) {
            this.head.center = new Vector3(newPos.x - head.transform.position.x, newPos.y - head.transform.position.y, 0);
        }
    }

    void OnEnable()
    {
        this.head = (KiteHead) target;
        if (this.head.center == null) {
            this.head.center = Vector3.zero;
        }
    }

}
