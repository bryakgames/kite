using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCollector : MonoBehaviour
{

    public ScoreHolder holder;
    
    void OnCollisionEnter2D(Collision2D collisionInfo)
    {
        var other = collisionInfo.collider;
        ScoreCollectable collectable = other.GetComponent<ScoreCollectable>();
        if (collectable == null)
        {
            return;
        }
        this.holder.currentScore += collectable.score;
        Destroy(other.gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        ScoreCollectable collectable = other.GetComponent<ScoreCollectable>();
        if (collectable == null)
        {
            return;
        }
        this.holder.currentScore += collectable.score;
        Destroy(other.gameObject);        
    }
}
