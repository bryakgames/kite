using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreDisplay : MonoBehaviour
{
    private TMP_Text text;

    public ScoreHolder holder;
    // Start is called before the first frame update
    void Start()
    {
        this.text = gameObject.GetComponent<TMP_Text>();
    }

    // Update is called once per frame
    void Update()
    {
        this.text.text = "Score: " + holder.currentScore;
    }
}
