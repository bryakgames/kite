using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudBehaviour : MonoBehaviour
{

    [SerializeField] public float horizontalSpeed;
    [SerializeField] public float cloudSize;

    private void Start() {
        transform.localScale = Vector3.one * cloudSize;
    }

    private void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * horizontalSpeed);
    }

    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
