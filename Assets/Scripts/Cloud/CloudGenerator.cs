using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudGenerator : MonoBehaviour
{

    [SerializeField] CloudBehaviour cloud;
    [SerializeField] int maximumClouds = 5;
    [SerializeField] float spawnCooldownSeconds = 5;

    [SerializeField] float minimalSpeed = 0.25f;
    [SerializeField] float maximalSpeed = 2;

    [SerializeField] float minimalSize = 1;
    [SerializeField] float maximalSize = 3;

    private List<CloudBehaviour> spawnedInstances = new List<CloudBehaviour>();
    private float spawnCooldownCounter;

    // Update is called once per frame
    void Update()
    {
        this.spawnedInstances.RemoveAll(cloud => cloud == null);
        if (this.spawnCooldownCounter > 0) {
            this.spawnCooldownCounter -= Time.deltaTime;
        }
        if (this.spawnCooldownCounter <=0 && this.spawnedInstances.Count < this.maximumClouds) {
            this.spawnedInstances.Add(this.CreateCloud());
            this.spawnCooldownCounter = this.spawnCooldownSeconds;
        }
    }

    private CloudBehaviour CreateCloud() {
        var position = this.GenerateStartPosition();
        CloudBehaviour newCloud = Instantiate(this.cloud, position, this.transform.rotation);
        newCloud.horizontalSpeed = (position.x >= 0 ? -1 : 1) * Random.Range(this.minimalSpeed, this.maximalSpeed);
        newCloud.cloudSize = Random.Range(this.minimalSize, this.maximalSize);
        return newCloud;
    }

    private Vector3 GenerateStartPosition() {
        float height = Random.Range(0.25f, 1) * Screen.height;
        Vector3 position;
        position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, height, 0));
        position.z = 0;
        return position;
    }
}
